# Introduction

# FixxIT Polymer Challenge

### Welcome fellow coder!
![Fellow Coder](https://media.giphy.com/media/OIEhvGRByVrHO/giphy.gif)

#### Introduction
---
You have chosen to undertake the FixxIT Polymer Challenge. This is a challenge and not a test! It is not about passing or failing, but rather about us giving you a clean canvas that you can use to demonstrate how passionate you are about coding. Thus, go forth **champion** and prove to us that you have what it takes to survive the every day life as a code wielding bug slayer!

Are you ready?

![Yes Sir](https://media.giphy.com/media/RavXJWRY3veEw/giphy.gif)

If that is your response, or something similar, we should probably get down to the details!

#### Before you start
---

As you might have noticed already, we have provided you with a "seed project" to get things going. This should give you a good point to start from without you having to setup things from scratch. *A lazy developer is a good developer.*

A few things that you should know before you start:
- Please read through the entire challenge before starting.
- You have only one week to finish the challenge.
- Please do frequent and well commented commits.
- Commenting your code is not mandatory, but will be appreciated.
- It does not have to be the prettiest.
- You are welcome to use a different Polymer seed project, but please provide the original link.
- Not all phases listed below are mandatory.
- It is not a race to try and finish all phases.
- Please tag the final commit of each phase completed.

#### Challenge description:
---

###### Background
So, I have this buddy called Frikkie. He owns a second hand car garage that is called...wait for it...**Frikkie's cabs**, original right?

He would like you to create a website for his shop. These days Frikkie only sells his second hand cars online and could use a website of his own. With that said I guess we should do this in phases, because you know...agile and what not. So best you start at **phase 1** then!

###### Phase 1:
Lets start by creating a page that will display all of the cars that Frikkie has across all of his warehouses. Please sort the list according to *date_added* asc. In the project we added a file called *db.json* that contains all the second hand cars the Frikkie has available for sale.

###### Phase 2:
Now, allowing the user to click on any of the cars (that are licensed(true)) would make it a lot nicer. Once the user clicked on a car we could then also show a details page that displays things such as, the warehouse where it is stored and its location perhaps? It is up to you.

###### Phase 3:
Wow! Okay, we now have some good phunctionality! So lets take it one step further! Lets allow the user to add the car he is viewing to some sort of *shopping cart* so that he can easily checkout once he is done shopping. Oh, and maybe give him the total amount as well.

###### Phase 4:
Frikkie is also struggling with his admin. Can you maybe come up with an easy way that he can also see a list of the cars that are not licensed(false)?


#### Side notes:
---
- Kudos if you leave the project in working order.
- Kudos if you have unit test coverage.
- Kudos if you use es6.
- Kudos if you have no linting issues.
- Kudos if you use Redux to manage your shopping cart.

#### Getting started:
---
To get started, please fork this repository and create a branch with the same name as the key that was given to you via email. If you have not received a key yet, please contact <maruschka@fixx.it> or me <francois.vandermerwe@fixx.it>. Once you have finished the challenge or you have ran out of time, please put in a merge request to the develop branch of this repository.

After we had time to review your awesome solution we will get back to you.

If there is something that you do not know then we suggest that you take the opportunity and

![Google IT](https://media.giphy.com/media/XPmVwGTPC149a/giphy.gif)


##### Good luck and have fun!


## Getting Started

### Install Dependencies

We have two kinds of dependencies in this project: tools and Angular framework code. The tools help
us manage and test the application.

* We get the tools we depend upon via `npm`, the [Node package manager][npm].
* We get the Angular code via `bower`, a [client-side code package manager][bower].
* In order to run the end-to-end tests, you will also need to have the
  [Java Development Kit (JDK)][jdk] installed on your machine. Check out the section on
  [end-to-end testing](#e2e-testing) for more info.

We have preconfigured `npm` to automatically run `bower` so we can simply do:

```
npm install
```

Behind the scenes this will also call `bower install`. After that, you should find out that you have
two new folders in your project.

* `node_modules` - contains the npm packages for the tools we need
* `app/bower_components` - contains the Angular framework files

*Note that the `bower_components` folder would normally be installed in the root folder but
`angular-seed` changes this location through the `.bowerrc` file. Putting it in the `app` folder
makes it easier to serve the files by a web server.*

### Run the Application

We have preconfigured the project with a simple development web server. The simplest way to start
this server is:

```
npm start
```

Now browse to the app at [`localhost:8000/index.html`][local-app-url].


## Testing

There are two kinds of tests in the `angular-seed` application: Unit tests and end-to-end tests.

### Running Unit Tests

The `angular-seed` app comes preconfigured with unit tests. These are written in [Jasmine][jasmine],
which we run with the [Karma][karma] test runner. We provide a Karma configuration file to run them.

* The configuration is found at `karma.conf.js`.
* The unit tests are found next to the code they are testing and have an `_test.js` suffix (e.g.
  `view1_test.js`).

The easiest way to run the unit tests is to use the supplied npm script:

```
npm test
```

This script will start the Karma test runner to execute the unit tests. Moreover, Karma will start
watching the source and test files for changes and then re-run the tests whenever any of them
changes.
This is the recommended strategy; if your unit tests are being run every time you save a file then
you receive instant feedback on any changes that break the expected code functionality.

You can also ask Karma to do a single run of the tests and then exit. This is useful if you want to
check that a particular version of the code is operating as expected. The project contains a
predefined script to do this:

```
npm run test-single-run
```


<a name="e2e-testing"></a>
### Running End-to-End Tests

The `angular-seed` app comes with end-to-end tests, again written in [Jasmine][jasmine]. These tests
are run with the [Protractor][protractor] End-to-End test runner. It uses native events and has
special features for Angular applications.

* The configuration is found at `e2e-tests/protractor-conf.js`.
* The end-to-end tests are found in `e2e-tests/scenarios.js`.

Protractor simulates interaction with our web app and verifies that the application responds
correctly. Therefore, our web server needs to be serving up the application, so that Protractor can
interact with it.

**Before starting Protractor, open a separate terminal window and run:**

```
npm start
```

In addition, since Protractor is built upon WebDriver, we need to ensure that it is installed and
up-to-date. The `angular-seed` project is configured to do this automatically before running the
end-to-end tests, so you don't need to worry about it. If you want to manually update the WebDriver,
you can run:

```
npm run update-webdriver
```

Once you have ensured that the development web server hosting our application is up and running, you
can run the end-to-end tests using the supplied npm script:

```
npm run protractor
```

This script will execute the end-to-end tests against the application being hosted on the
development server.

**Note:**
Under the hood, Protractor uses the [Selenium Standalone Server][selenium], which in turn requires
the [Java Development Kit (JDK)][jdk] to be installed on your local machine. Check this by running
`java -version` from the command line.

If JDK is not already installed, you can download it [here][jdk-download].


## Updating Angular

Since the Angular framework library code and tools are acquired through package managers (npm and
bower) you can use these tools to easily update the dependencies. Simply run the preconfigured
script:

```
npm run update-deps
```

This will call `npm update` and `bower update`, which in turn will find and install the latest
versions that match the version ranges specified in the `package.json` and `bower.json` files
respectively.


## Loading Angular Asynchronously

The `angular-seed` project supports loading the framework and application scripts asynchronously.
The special `index-async.html` is designed to support this style of loading. For it to work you must
inject a piece of Angular JavaScript into the HTML page. The project has a predefined script to help
do this:

```
npm run update-index-async
```

This will copy the contents of the `angular-loader.js` library file into the `index-async.html`
page. You can run this every time you update the version of Angular that you are using.


## Serving the Application Files

While Angular is client-side-only technology and it is possible to create Angular web apps that
do not require a backend server at all, we recommend serving the project files using a local
web server during development to avoid issues with security restrictions (sandbox) in browsers. The
sandbox implementation varies between browsers, but quite often prevents things like cookies, XHR,
etc to function properly when an HTML page is opened via the `file://` scheme instead of `http://`.

### Running the App during Development

The `angular-seed` project comes preconfigured with a local development web server. It is a Node.js
tool called [http-server][http-server]. You can start this web server with `npm start`, but you may
choose to install the tool globally:

```
sudo npm install -g http-server
```

Then you can start your own development web server to serve static files from a folder by running:

```
http-server -a localhost -p 8000
```

Alternatively, you can choose to configure your own web server, such as Apache or Nginx. Just
configure your server to serve the files under the `app/` directory.

### Running the App in Production

This really depends on how complex your app is and the overall infrastructure of your system, but
the general rule is that all you need in production are the files under the `app/` directory.
Everything else should be omitted.

Angular apps are really just a bunch of static HTML, CSS and JavaScript files that need to be hosted
somewhere they can be accessed by browsers.

If your Angular app is talking to the backend server via XHR or other means, you need to figure out
what is the best way to host the static files to comply with the same origin policy if applicable.
Usually this is done by hosting the files by the backend server or through reverse-proxying the
backend server(s) and web server(s).


